/**
 * Created by Kyia on 13-10-28.
 */

var WelcomeLayer = cc.Layer.extend({

	init: function() {
		var bRet = false;
		if (this._super()) {
			var itemStart = cc.MenuItemImage.create(
				s_btnStartNormal,
				s_btnStartDown,
				this.onStartGame,
				this
			);
			itemStart.setPosition(cc.p(240, 240));

			var menu = cc.Menu.create(itemStart);
			menu.setPosition(cc.p(0, 0));
			this.addChild(menu);

			bRet = true;
		}
		return bRet;
	},

	onStartGame: function() {
		var scene = cc.Scene.create();
		scene.addChild(SushiMatrix.create());
		cc.Director.getInstance().replaceScene(cc.TransitionSlideInT.create(0.6, scene));
	}
});

WelcomeLayer.create = function() {
	var sg = new WelcomeLayer();
	if (sg && sg.init()) {
		return sg;
	}
	return null;
};

WelcomeLayer.scene = function() {
	var scene = cc.Scene.create();
	var layer = WelcomeLayer.create();
	scene.addChild(layer);
	return scene;
};
