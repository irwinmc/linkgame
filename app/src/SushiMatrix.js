/**
 * Created by Kyia on 13-10-28.
 */

var g_sharedSushiMatrix;

var SushiMatrix = cc.Layer.extend({
	// Attributes
	patternBatchNode: null,
	matrixRow: 0,
	matrixCol: 0,
	patternTypeMax: 0,

	_gameScore: 0,
	_leftPattern: 0,
	_timeTally: 0,
	_timeTotal: 0,
	_stage: 0,

	// Array....
	gameStage: null,
	patternSprites: null,		// Sprite array...
	patternPositions: null,		// All the axis position (x, y)
	patternTypes: null,			// Contains the all pattern type, and need to generate random order
	selectedPattern: null,		// Selected pattern

	// Controls
	_scoreLabel: null,
	_leftPatternLabel: null,
	_progressBarBgSprite: null,
	_progressBarSprite: null,
	_visibleRect: null,

	init: function() {
		var bRet = false;
		if (this._super()) {
			// Add sushi blocks
			cc.SpriteFrameCache.getInstance().addSpriteFrames(s_sushi_plist);

			this.patternTypeMax = SS.PATTERN.NUM;

			this.matrixRow = SS.MATRIX.ROW;
			this.matrixCol = SS.MATRIX.COL;

			this._timeTotal = 60;
			this._stage = 0;

			this.initProgressBar();
			this.initLabels();

			this.patternBatchNode = cc.SpriteBatchNode.create(s_sushi, this.matrixRow * this.matrixCol * 2);
			this.addChild(this.patternBatchNode, 1);

			this.gameStage = eval('Level' + this._stage);
			this.runAction(cc.Sequence.create(cc.DelayTime.create(0.7), cc.CallFunc.create(this.initMatrix.bind(this), this)));
			// this.initMatrix();

			bRet = true;

			g_sharedSushiMatrix = this;
		}
		return bRet;
	},

	initProgressBar: function() {
		this._progressBarBgSprite = cc.Sprite.create(s_progressBarBack);
		this._progressBarBgSprite.setAnchorPoint(cc.p(0.0, 0.5));
		this._progressBarBgSprite.setPosition(cc.p(170, 50));
		this.addChild(this._progressBarBgSprite);

		this._visibleRect = cc.rect(0, 0, 257, 19);

		this._progressBarSprite = cc.Sprite.create(s_progressBarFront);
		this._progressBarSprite.setAnchorPoint(cc.p(0.0, 0.5));
		this._progressBarSprite.setPosition(cc.p(170, 50));
		this._progressBarSprite.setTextureRect(this._visibleRect);
		this.addChild(this._progressBarSprite);
	},

	initLabels: function() {
		this._scoreLabel = cc.LabelTTF.create(SS.LABEL.SCORE + ' ' + this._gameScore, 'Arial', 20);
		this._scoreLabel.setAnchorPoint(cc.p(0, 0));
		this._scoreLabel.setPosition(cc.p(60, 750));

		this.addChild(this._scoreLabel);

		this._leftPatternLabel = cc.LabelTTF.create(SS.LABEL.LEFT + ' ' + this._leftPattern, 'Arial', 20);
		this._leftPatternLabel.setAnchorPoint(cc.p(0, 0));
		this._leftPatternLabel.setPosition(cc.p(200, 750));
		this.addChild(this._leftPatternLabel);
	},

	initMatrix: function() {
		this.patternSprites = this.createIntArray(this.matrixRow, this.matrixCol, null);
		this.patternPositions = this.createIntArray(this.matrixRow, this.matrixCol, null);
		this.patternTypes = this.createIntArray(this.matrixRow, this.matrixCol, -1);

		var halfSpace = SS.PATTERN.HALF_WIDTH;
		var space = halfSpace * 2;
		var baseX = SS.RESOLUTION.HALF_WIDTH + halfSpace - this.matrixCol * halfSpace;
		var baseY = SS.RESOLUTION.HALF_HEIGHT + halfSpace - this.matrixRow * halfSpace;

		var row, col;

		for (row = 0; row < this.matrixRow; row++) {
			for (col = 0; col < this.matrixCol; col++) {
				this.patternPositions[row][col] = cc.p(baseX + col * space, baseY + row * space);
			}
		}

		for (row = 0; row < this.matrixRow; row++) {
			for (col = 0; col < this.matrixCol / 2; col++) {
				if (this.gameStage[row][col] == 1) {
					this.patternTypes[row][col] = Math.floor(Math.random() * this.patternTypeMax)
					this.patternTypes[row][this.matrixCol - 1 - col] = this.patternTypes[row][col];
				}
			}
		}

		// Shuffle!!!
		this.randomPatternArray();

		for (row = 0; row < this.matrixRow; row++) {
			for (col = 0; col < this.matrixCol; col++) {
				this.addPattern(row, col, this.patternTypes[row][col]);
			}
		}

		// Update left pattern count label
		this.updateLeftPattern();
		this._timeTally = 0;

		// Schedule the update
		this.schedule(this.updateTimer, 0.15);
	},

	/**
	 * Pattern random.... to array
	 */
	randomPatternArray: function() {
		// Make some map, the 1 means can be set
		// New temp array
		var array = new Array();
		// Add to array
		for (var row = 0; row < this.matrixRow; row++) {
			for (var col = 0; col < this.matrixCol; col++) {
				if (this.patternTypes[row][col] != -1) {
					array.push(this.patternTypes[row][col]);
				}
			}
		}

		// Random swap twice
		var length = array.length;
		for (var j = 0; j < 2; j++) {
			for (var i = 0; i < length; i++) {
				var s = Math.floor(Math.random() * length);
				var t = array[i];
				array[i] = array[s];
				array[s] = t;
			}
		}

		// To the random type
		for (var row = 0; row < this.matrixRow; row++) {
			for (var col = 0; col < this.matrixCol; col++) {
				if (this.gameStage[row][col] == 1) {
					this.patternTypes[row][col] = array.pop();
				}
			}
		}
	},

	/**
	 * Shuffle the matrix when there has no solution
	 */
	shuffleMatrix: function() {

	},

	/**
	 * Add a pattern
	 *
	 * @param row
	 * @param col
	 * @param type
	 */
	addPattern: function(row, col, type) {
		if (type == -1) return;

		this.patternSprites[row][col] = new Sushi(type);
		this.patternSprites[row][col].setAnchorPoint(cc.p(0.5, 0.5));
		this.patternSprites[row][col].rowIndex = row;
		this.patternSprites[row][col].colIndex = col;
		this.patternSprites[row][col].setPosition(this.patternPositions[row][col].x, this.patternPositions[row][col].y + 800);
		this.patternSprites[row][col].moveTo(0.5, this.patternPositions[row][col]);

		this.patternBatchNode.addChild(this.patternSprites[row][col]);
	},

	/**
	 * Remove a pattern
	 *
	 * @param pattern
	 */
	removePattern: function(pattern) {
		// Destroy this pattern
		pattern.destroy();

		// Update sprite array
		this.patternSprites[pattern.rowIndex][pattern.colIndex] = null;
//		this.patternBatchNode.removeChild(pattern, true);
	},

	/**
	 * Used in {@Link Sushi.js}
	 *
	 * @param pattern
	 */
	onCheckPattern: function(pattern) {
		if (pattern != null) {
			// No one selected
			if (!this.selectedPattern) {
				// Make selected
				this.selectedPattern = pattern;
			} else if (this.selectedPattern.rowIndex == pattern.rowIndex && this.selectedPattern.colIndex == pattern.colIndex) {
				// Same pattern do nothing
			} else {
				if (this.selectedPattern.patternType == pattern.patternType) {
					// CHECK
					if (this.checkTwoPatterns(this.selectedPattern, pattern)) {
						// Clear
						this.afterCheckTwoPatterns(this.selectedPattern, pattern);
						// Clear selected
						this.selectedPattern = null;
					} else {
						this.selectedPattern = null;
					}
				} else {
					// Make selected
					this.selectedPattern = pattern;
				}
			}
		}
	},

	/**
	 * Link two pattern, check the line between the two patterns, check
	 * the line to 3 at most
	 *
	 * @param firstPattern
	 * @param secondPattern
	 * @returns {boolean}
	 */
	checkTwoPatterns: function(firstPattern, secondPattern) {
		var fpRow, fpCol, spRow, spCol;
		fpRow = firstPattern.rowIndex;
		fpCol = firstPattern.colIndex;
		spRow = secondPattern.rowIndex;
		spCol = secondPattern.colIndex;

		var a = new SS.Point(fpCol, fpRow);
		var b = new SS.Point(spCol, spRow);

		// console.log('a:{' + a.x + ', ' + a.y + '}');
		// console.log('b:{' + b.x + ', ' + b.y + '}');

		if (a.x == b.x && this.checkHorizon(a, b)) {
			return true;
		}
		if (a.y == b.y && this.checkVertical(a, b)) {
			return true;
		}
		if (this.checkOneCorner(a, b)) {
			return true;
		}
		if (this.checkTwoCorners(a, b)) {
			return true;
		}
		return false;
	},

	/**
	 * Remove the pattern after check
	 *
	 * @param firstPattern
	 * @param secondPattern
	 */
	afterCheckTwoPatterns: function(firstPattern, secondPattern) {
		// Draw link line
		// this.drawLinkLine(firstPattern, secondPattern);

		// Remove the pattern
		this.removePattern(firstPattern);
		this.removePattern(secondPattern);

		// Update score
		this.updateScore();
		this.updateLeftPattern();

		// Update time
		this._timeTally -= 1;

		// Check game over
		if (this._leftPattern == 0) {
			this.stopGame();

			// Max stage
			this._stage++;
			if (this._stage > 5) {
				this.showGameResult();
			} else {
				// Stage forward
				this.gameStage = eval('Level' + this._stage);
				this.runAction(cc.Sequence.create(cc.DelayTime.create(1), cc.CallFunc.create(this.initMatrix.bind(this), this)));

				// Next level
				// this.initMatrix(eval('Level' + this._stage));
			}
		}
	},

	/**
	 * Draw line
	 *
	 * @param firstPattern
	 * @param secondPattern
	 */
	drawLinkLine: function(firstPattern, secondPattern) {
		var first = this.patternPositions[firstPattern.rowIndex][firstPattern.colIndex];
		var second = this.patternPositions[secondPattern.rowIndex][secondPattern.colIndex];

		// Draw line
		cc.drawingUtil.setDrawColor4B(255, 0, 0, 255);
		cc.drawingUtil.setLineWidth(5);
		cc.drawingUtil.drawLine(first, second);
	},

	/**
	 * a(1,2) , b(1,7)
	 *
	 * @param a
	 * @param b
	 * @returns {boolean}
	 */
	checkHorizon: function(a, b) {
		if (a.x == b.x && a.y == b.y)
			return false;

		var startY = a.y < b.y ? a.y : b.y;
		var endY = a.y < b.y ? b.y : a.y;
		for (var i = startY + 1; i < endY; i++) {
			if (this.patternSprites[i][a.x] != null) {
				return false;
			}
		}
		return true;
	},

	/**
	 * a(1,1) , b(4,1)
	 *
	 * @param a
	 * @param b
	 * @returns {boolean}
	 */
	checkVertical: function(a, b) {
		if (a.x == b.x && a.y == b.y)
			return false;

		var startX = a.x < b.x ? a.x : b.x;
		var endX = a.x < b.x ? b.x : a.x;
		for (var i = startX + 1; i < endX; i++) {
			if (this.patternSprites[a.y][i] != null) {
				return false;
			}
		}
		return true;
	},

	/**
	 * a(4,2) , b(2,7)
	 * c(2,2) , d(4,7)
	 *
	 * @param a
	 * @param b
	 * @returns {*}
	 */
	checkOneCorner: function(a, b) {
		var c = new SS.Point(b.x, a.y);
		var d = new SS.Point(a.x, b.y);

		if (this.patternSprites[c.y][c.x] == null) {
			return this.checkHorizon(b, c) && this.checkVertical(a, c);
		}
		if (this.patternSprites[d.y][d.x] == null) {
			return this.checkHorizon(a, d) && this.checkVertical(b, d);
		}
		return false;
	},

	/**
	 * Check two corners
	 *
	 * @param a
	 * @param b
	 */
	checkTwoCorners: function(a, b) {
		var lineArray = this.scanLine(a, b);
		if (lineArray.length == 0) {
			return false;
		}

		for (var i = 0; i < lineArray.length; i++) {
			var tempLine = lineArray[i];
			if (tempLine.direct == 1) {
				if (this.checkVertical(a, tempLine.a) && this.checkVertical(b, tempLine.b)) {
					return true;
				}
			} else if (tempLine.direct == 0) {
				if (this.checkHorizon(a, tempLine.a) && this.checkHorizon(b, tempLine.b)) {
					return true;
				}
			}
		}

		return false;
	},

	/**
	 * Scan line
	 * y
	 * |
	 * |
	 * |
	 * |—————————— x
	 *0,0
	 * @param a
	 * @param b
	 * @returns {Array}
	 */
	scanLine: function(a, b) {
		var lineArray = new Array();
		var i, j;

		// Check left
		for (i = a.y; i >= 0; i--) {
			if (this.patternSprites[i][a.x] == null && this.patternSprites[i][b.x] == null &&
				this.checkVertical(new SS.Point(a.x, i), new SS.Point(b.x, i))) {
				lineArray.push(new SS.Line(new SS.Point(a.x, i), new SS.Point(b.x, i), 0));
			}
		}

		// Check right
		for (i = a.y; i < this.matrixRow; i++) {
			if (this.patternSprites[i][a.x] == null && this.patternSprites[i][b.x] == null &&
				this.checkVertical(new SS.Point(a.x, i), new SS.Point(b.x, i))) {
				lineArray.push(new SS.Line(new SS.Point(a.x, i), new SS.Point(b.x, i), 0));
			}
		}

		// Check bottom
		for (j = a.x; j >= 0; j--) {
			if (this.patternSprites[a.y][j] == null && this.patternSprites[b.y][j] == null &&
				this.checkHorizon(new SS.Point(j, a.y), new SS.Point(j, b.y))) {
				lineArray.push(new SS.Line(new SS.Point(j, a.y), new SS.Point(j, b.y), 1));
			}
		}

		// Check top
		for (j = a.x; j < this.matrixCol; j++) {
			if (this.patternSprites[a.y][j] == null && this.patternSprites[b.y][j] == null &&
				this.checkHorizon(new SS.Point(j, a.y), new SS.Point(j, b.y))) {
				lineArray.push(new SS.Line(new SS.Point(j, a.y), new SS.Point(j, b.y), 1));
			}
		}

		// console.log(lineArray);
		return lineArray;
	},

	/**
	 * Update score
	 */
	updateScore: function() {
		this._gameScore += 2 * 100;
		this._scoreLabel.setString(SS.LABEL.SCORE + ' ' + this._gameScore)
	},

	/**
	 * Update left pattern
	 */
	updateLeftPattern: function() {
		// Check game end
		var left = 0;
		for (var row = 0; row < this.matrixRow; row++) {
			for (var col = 0; col < this.matrixCol; col++) {
				if (this.patternSprites[row][col] != null) {
					left += 1;
				}
			}
		}
		this._leftPattern = left;
		this._leftPatternLabel.setString(SS.LABEL.LEFT + ' ' + this._leftPattern);
	},

	/**
	 * Update timer
	 */
	updateTimer: function(dt) {
		this._timeTally += dt;
		this.updateProgressBar();
	},

	updateProgressBar: function() {
		var percent = (this._timeTotal - this._timeTally) / this._timeTotal;
		if (percent > 1.0) {
			percent = 1.0;
		} else if (percent < 0.0) {
			percent = 0.0;
		}

		var vw = percent * 257;
		this._visibleRect = cc.rect(0, 0, vw, 19);
		this._progressBarSprite.setTextureRect(this._visibleRect);

		if (percent === 0.0) {
			this.showGameResult();
		}
	},

	stopGame: function() {
		this.unscheduleAllCallbacks();
	},

	showGameResult: function() {
		this.stopGame();

		var resultLayer = ResultLayer.create();

		this.onExit();

		cc.Director.getInstance().getRunningScene().addChild(resultLayer, 99);
	},

	/**
	 * Utils method
	 *
	 * @param row
	 * @param col
	 * @param value
	 * @returns {Array}
	 */
	createIntArray: function(row, col, value) {
		var array = [];
		for (var i = 0; i < row; i++) {
			array[i] = [];
			for (var j = 0; j < col; j++) {
				array[i][j] = value;
			}
		}
		return array;
	}
});

SushiMatrix.create = function () {
	var sg = new SushiMatrix();
	if (sg && sg.init()) {
		return sg;
	}
	return null;
};

SushiMatrix.scene = function () {
	var scene = cc.Scene.create();
	var layer = SushiMatrix.create();
	scene.addChild(layer, 1);
	return scene;
};

