/**
 * Created by Kyia on 13-10-28.
 */

// Stage
SS.RESOLUTION = {
	HALF_WIDTH: 240,
	HALF_HEIGHT: 400
};

// Keys
SS.KEYS = [];

// Matrix
SS.MATRIX = {
	ROW: 12,
	COL: 8
};

// Pattern
SS.PATTERN = {
	NUM: 20,
	HALF_WIDTH: 24,
	HALF_HEIGHT: 24
};

SS.PATTERN_EXTRA = {
	NORMAL: 0,
	BOMB: 1,
	FREEZE: 2
};

// Pattern status
SS.PATTERN_STATUS = {
	NORMAL: 0,
	MOVE: 1,
	DESTROY: 2,
	EXPLODE: 3
};

// Level
SS.LEVEL = {
	STAGE1: 1,
	STAGE2: 2,
	STAGE3: 3
};

// Time
SS.TIME_IN_SECONDS = 120;

// Score
SS.SCORE = 0;

// Help
SS.HELP = {
	HINT: 5,
	RESET: 3
};

// Sound
SS.SOUND = true;

// Label
SS.LABEL = {
	SCORE: '分数',
	LEFT: '剩余'
};
