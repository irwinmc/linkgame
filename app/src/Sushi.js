/**
 * Created by Kyia on 13-10-28.
 */

var Sushi = cc.Sprite.extend({
	_active: true,
	_status: SS.PATTERN_STATUS.NORMAL,
	_extra: SS.PATTERN_EXTRA.NORMAL,

	patternType: -1,
	rowIndex: 0,
	colIndex: 0,
	removeBatchIndex: 0,

	ctor: function(type) {
		this._super();
		this.init(type);
	},

	init: function(type) {
		var bRet = false;
		if (this._super()) {
			// Pattern type
			this.patternType = type;

			// A cc.SpriteFrame will be fetched from the cc.SpriteFrameCache by name.
			var str = SushiType[type].textureName;
			this.initWithSpriteFrameName(str);

			bRet = true;
		}
		return bRet;
	},

	// Normal
	destroy: function() {
		this.setVisible(false);
		this._active = false;
	},

	// Bomb
	explode: function() {

	},

	removeFreeze: function() {

	},

	moveTo: function(duration, position) {
		if (this._status == SS.PATTERN_STATUS.NORMAL) {
			this._status = SS.PATTERN_STATUS.MOVE;
			var action = cc.Sequence.create(cc.MoveTo.create(duration, position), cc.CallFunc.create(this.onMoveEnd.bind(this), this));
			this.runAction(action);
		}
	},

	onMoveEnd: function() {
		this._status = SS.PATTERN_STATUS.NORMAL;
	},

	checkTouchLocation: function(touch) {
		var getPoint = touch.getLocation();
		var lx = 0 | (getPoint.x -  this.getPosition().x);	//this.getPositionX();
		var ly = 0 | (getPoint.y -  this.getPosition().y);	//this.getPositionY();
		if (lx > -23.5 && lx < 23.5 && ly > -23.5 && ly < 23.5) {
			return true;
		}
		return false;
	},

	onTouchBegan: function(touch, event) {
		if (this._active && this.checkTouchLocation(touch)) {
			// Make some effect
			// this.runAction(cc.Sequence.create(cc.DelayTime.create(0.5), cc.Blink.create(1, 3)));

			// console.log('x: ' + this.rowIndex + ', y:' + this.colIndex);
			g_sharedSushiMatrix.onCheckPattern(this);
		}
	},

	onTouchEnded: function(touch, event) {

	},

	onEnter: function() {
		if (sys.platform == "browser") {
			cc.registerTargetedDelegate(1, true, this);
		} else {
			cc.registerTargetedDelegate(1, true, this);
		}
		this._super();
	},

	onExit: function() {
		cc.unregisterTouchDelegate(this);
		this._super();
	}
});

