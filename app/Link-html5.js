var SS = SS || {};

(function () {
	var d = document;
	var c = {
		COCOS2D_DEBUG: 2,
		showFPS: true,
		frameRate: 60,
		loadExtension: false,
		renderMode: 1,
		tag: 'gameCanvas',
		engineDir: '../cocos2d/',
		appFiles: [
			'src/Resource.js',
			'src/config/GameConfig.js',
			'src/config/Level.js',
			'src/config/SushiType.js',
			'src/WelcomeLayer.js',
			'src/Sushi.js',
			'src/SushiMatrix.js',
			'src/ResultLayer.js'
		]
	};

	window.addEventListener('DOMContentLoaded', function () {
		//first load engine file if specified
		var s = d.createElement('script');
		s.src = c.engineDir + 'platform/jsloader.js';
		document.ccConfig = c;
		s.id = 'cocos2d-html5';
		d.body.appendChild(s);
	});
})();
