/**
 * Created by Kyia on 13-10-30.
 */

var ResultLayer = cc.Layer.extend({
	_gameScore: 0,

	init: function() {
		var bRet = false;
		if (this._super()) {
			// Result panel
			var resultPanel = cc.Sprite.create(s_panResult, cc.rect(0, 0, 384, 576));
			resultPanel.setAnchorPoint(cc.p(0.5, 0.5));
			resultPanel.setPosition(cc.p(240, 400));
			this.addChild(resultPanel);

			// Restart button
			var itemRestart = cc.MenuItemImage.create(
				s_btnRestartNormal,
				s_btnRestartDown,
				this.onRestartGame,
				this
			);
			itemRestart.setPosition(cc.p(192, 56));

			var menu = cc.Menu.create(itemRestart);
			menu.setPosition(cc.p(0, 0));
			resultPanel.addChild(menu);

			bRet = true;
		}
		return bRet;
	},

	onRestartGame: function() {
		var scene = cc.Scene.create();
		scene.addChild(SushiMatrix.create());
		cc.Director.getInstance().replaceScene(cc.TransitionSlideInT.create(0.6, scene));
	}
});

ResultLayer.create = function () {
	var sg = new ResultLayer();
	if (sg && sg.init()) {
		return sg;
	}
	return null;
};

ResultLayer.scene = function () {
	var scene = cc.Scene.create();
	var layer = ResultLayer.create();
	scene.addChild(layer);
	return scene;
};