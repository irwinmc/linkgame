var dirRes = "res/";
var btnDirRes = dirRes + "btn/";

// Images
var s_sushi = dirRes + "sushi.png";
var s_star = dirRes + "star.png";

// Btn
var s_btnStartNormal = btnDirRes + "btnStartNormal.png";
var s_btnStartDown = btnDirRes + "btnStartDown.png";
var s_btnRestartNormal = btnDirRes + "btnRestartNormal.png";
var s_btnRestartDown = btnDirRes + "btnRestartDown.png";

var s_progressBarBack = dirRes + "progressBarBack.png";
var s_progressBarFront = dirRes + "progressBarFront.png";

var s_panResult = dirRes + "panResult.png";

// Plist
var s_sushi_plist = dirRes + "sushi.plist";

var g_resources = [
	// Image
	{src: s_sushi},
	{src: s_star},
	{src: s_btnStartNormal},
	{src: s_btnStartDown},
	{src: s_btnRestartNormal},
	{src: s_btnRestartDown},
	{src: s_progressBarBack},
	{src: s_progressBarFront},
	{src: s_panResult},

	// Plist
	{src: s_sushi_plist}
];

/**
 * Point
 *
 * @type {extend|*}
 */
SS.Point = cc.Class.extend({
	x: 0,
	y: 0,
	ctor: function(x, y) {
		this.x = x;
		this.y = y;
	}
});

/**
 * Line
 *
 * @type {extend|*}
 */
SS.Line = cc.Class.extend({
	a: null,
	b: null,
	direct: 0,
	ctor: function(a, b, direct) {
		this.a = a;
		this.b = b;
		this.direct = direct;
	}
});