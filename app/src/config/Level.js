/**
 * Created by Kyia on 13-10-30.
 */

var DefaultLevel = new Array(
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 0, 0, 0, 0, 0, 0, 0]
);

var Level0 = new Array(
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0]
);

var Level1 = new Array(
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 0, 0, 0, 0, 0, 0, 0]
);

var Level2 = new Array(
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 1, 1, 1, 1, 0, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 0, 1, 1, 1, 1, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 1, 1, 1, 1, 0, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 0, 1, 1, 1, 1, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0]
);

var Level3 = new Array(
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 0, 1, 1, 1, 1, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 0, 1, 1, 0, 0, 0],
	[0, 0, 1, 1, 1, 1, 0, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 0, 0, 0, 0, 0, 0, 0]
);

var Level4 = new Array(
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 1, 0, 1, 1, 0, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 0, 1, 1, 0, 1, 0],
	[0, 1, 0, 1, 1, 0, 1, 0],
	[0, 1, 0, 1, 1, 0, 1, 0],
	[0, 1, 0, 1, 1, 0, 1, 0],
	[0, 1, 0, 0, 0, 0, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 0, 0, 0, 0, 0, 0, 0]
);

var Level5 = new Array(
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 0, 0, 0, 0, 1, 0],
	[0, 1, 0, 0, 0, 0, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 1, 0, 0, 1, 1, 0],
	[0, 1, 0, 0, 0, 0, 1, 0],
	[0, 1, 0, 0, 0, 0, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 1, 1, 1, 1, 1, 1, 0],
	[0, 0, 0, 0, 0, 0, 0, 0]
);