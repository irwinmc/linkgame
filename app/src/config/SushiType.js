/**
 * Created by Kyia on 13-10-28.
 */

var SushiType = [
	{
		type: 0,
		textureName: 'Sushi01.png'
	},
	{
		type: 1,
		textureName: 'Sushi02.png'
	},
	{
		type: 2,
		textureName: 'Sushi03.png'
	},
	{
		type: 3,
		textureName: 'Sushi04.png'
	},
	{
		type: 4,
		textureName: 'Sushi05.png'
	},
	{
		type: 5,
		textureName: 'Sushi06.png'
	},
	{
		type: 6,
		textureName: 'Sushi07.png'
	},
	{
		type: 7,
		textureName: 'Sushi08.png'
	},
	{
		type: 8,
		textureName: 'Sushi09.png'
	},
	{
		type: 9,
		textureName: 'Sushi10.png'
	},
	{
		type: 10,
		textureName: 'Sushi11.png'
	},
	{
		type: 11,
		textureName: 'Sushi12.png'
	},
	{
		type: 12,
		textureName: 'Sushi13.png'
	},
	{
		type: 13,
		textureName: 'Sushi14.png'
	},
	{
		type: 14,
		textureName: 'Sushi15.png'
	},
	{
		type: 15,
		textureName: 'Sushi16.png'
	},
	{
		type: 16,
		textureName: 'Sushi17.png'
	},
	{
		type: 17,
		textureName: 'Sushi18.png'
	},
	{
		type: 18,
		textureName: 'Sushi19.png'
	},
	{
		type: 19,
		textureName: 'Sushi20.png'
	}
]