require("jsb.js");

var SS = SS || {};

var appFiles = [
	'src/Resource.js',
	'src/config/GameConfig.js',
	'src/config/Level.js',
	'src/config/SushiType.js',
	'src/WelcomeLayer.js',
	'src/Sushi.js',
	'src/SushiMatrix.js',
	'src/ResultLayer.js'
];

cc.dumpConfig();

for (var i = 0; i < appFiles.length; i++) {
	require(appFiles[i]);
}

var director = cc.Director.getInstance();
director.setDisplayStats(true);

// set FPS. the default value is 1.0/60 if you don't call this
director.setAnimationInterval(1.0 / 60);

// create a scene. it's an autorelease object
var mainScene = WelcomeLayer.scene();

// run
director.runWithScene(mainScene);